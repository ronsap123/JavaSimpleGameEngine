package com.ronsap.test.gfx;

import com.ronsap.test.Art;

public class Sprite 
{
	public static final int CF = 0xffffff;
	//public static final int C1 = -3947581, C2 = -5131855, C3 = -8421505, CT = -16777216;
	public static final int C1 = (int) Long.parseLong("FFC3C3C3", 16),
			                C2 = (int) Long.parseLong("FFB1B1B1", 16),
			                C3 = (int) Long.parseLong("FF7F7F7F", 16),
			                CT = (int) Long.parseLong("FF000000", 16);
	
	
	public int tilex, tiley, swidth, sheight;
	public Art s;
	public int frame;
	public int c1, c2, c3;
	
	public Sprite(int c1, int c2, int c3, int tilex, int tiley, Art s, int width, int height)
	{
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.tilex = tilex;
		this.tiley = tiley;
		this.s = s;
		this.swidth = width;
		this.sheight = height;
	}
}
