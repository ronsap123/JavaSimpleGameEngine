package com.ronsap.test.level;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.ronsap.test.Screen;
import com.ronsap.test.units.entities.*;
import com.ronsap.test.units.map.*;

public class Level {
	
	private Screen s;
	private Map m;
	public Player p;
	private double xScroll = 0, yScroll = 0, ticks = 0;
	
	private List<Entity> entities = new LinkedList<Entity>();
	
	
	public Level(Screen s, Map m)
	{
		this.s = s;
		this.m = m;
		this.p = new Player(100,-50,15,15);
		for(int i = 0; i < 20; i++)
			entities.add(new Zombie(new Random().nextInt(500),new Random().nextInt(100)-100,15,15));
	}
	
	public void tick()
	{
		ticks += 2;
		if(ticks == 360)
			ticks = 0;
		m.handleEntityMovement(p);
		for(int i = 0; i < entities.size(); i++)
			m.handleEntityMovement(entities.get(i));
		p.tick();
		for(int i = 0; i < entities.size(); i++)
		{
			entities.get(i).tick();
			if(entities.get(i) instanceof Zombie)
			{
				if(p.feet.isColliding(entities.get(i)))
				{
					if(p.falling)
					{
						entities.remove(i);
						p.fall *= -0.87;
						if(p.fall > -0.05)
							p.fall = -0.05;
					}
				}
			}
		}
		xScroll = this.p.left.getX()  - s.getWidth()/2 + 15/2;
		yScroll = this.p.left.getY() - s.getHeight()/2 + 15/2;
		
	}
	
	public void render()
	{
		int x_player = (int)(p.getEx()/15.0);
		int y_player = (int)(p.getEy()/15.0);
		int sw = (int)(s.getWidth()/15.0);
		int sh = (int)(s.getHeight()/15.0);
		
		for(int i = x_player - sw/2-1; i <= x_player+sw/2+1; i++)
		{
			for(int j = y_player - sh/2-1; j <= y_player + sh/2+1; j++)
			{
				if(m.blockAt(i, j) == null)
					continue;
				s.drawSprite( m.blockAt(i, j), (int)(i*15-xScroll), (int)(j*15-yScroll), false);		
			}
		}

		for(int i = 0; i < entities.size(); i++)
			entities.get(i).render(s,  (int)xScroll, (int)yScroll);
		
		p.render(s, (int)xScroll, (int)yScroll);
	}
	
}
