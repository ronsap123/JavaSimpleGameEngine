package com.ronsap.test.units;

import java.awt.Rectangle;

import com.ronsap.test.units.entities.Entity;
import com.ronsap.test.units.map.Block;

public class CollisionBox extends Rectangle{

	private static final long serialVersionUID = 1L;
	
	public CollisionBox(int x, int y, int w, int h)
	{
		super(x,y,w,h);
	}
	
	public void setBounds(double x, double y, double width, double height)
	{
		super.setBounds((int)x,(int)y,(int)width,(int)height);
	}
	
	public boolean isColliding(Rectangle r)
	{
		if( ((this.x+this.width) >= r.x) && (this.x <= (r.x+r.width)) && ((this.y+this.height) >= r.y) && (this.y <= (r.y+r.height)))
		{
			return true;
		}
		return false;
	}
	
	public boolean isColliding(Entity e)
	{
		if( this.x+this.width >= e.getEx() && this.x <= (e.getEx()+e.getWidth()) &&
				this.y+this.height >= e.getEy() && this.y <= (e.getEy()+e.getHeight()))
		{
			return true;
		}
		return false;
	}
	
	public boolean isColliding(Block b)
	{
		if( this.x+this.width >= (b.getX()*b.getWidth())
				&& this.x <= ((b.getX()+1)*b.getWidth()) &&
				this.y+this.height >= (b.getY()*b.getHeight())
				&& this.y <= ((b.getY()+1)*b.getHeight()))
		{
			return true;
		}
		return false;
	}
	
}
