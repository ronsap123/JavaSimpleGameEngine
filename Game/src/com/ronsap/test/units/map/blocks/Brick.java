package com.ronsap.test.units.map.blocks;

import com.ronsap.test.units.map.Block;

public class Brick extends Block
{
	public Brick(int x, int y)
	{
		super(3,0,15,15,x,y,0.8f,0xffcca9bb,0xff826b77,0xff564a50);
	}
}
