package com.ronsap.test.units.entities;

import com.ronsap.test.Art;
import com.ronsap.test.Screen;
import com.ronsap.test.gfx.Sprite;
import com.ronsap.test.units.CollisionBox;

public class Entity extends Sprite{
	
	public int width,height;
	public double fall = 0.2f, acceleration = 0.1f, speed = 0.15f, xVel = 0.0f;
	public boolean moving_left, moving_right, jumping, falling;
	private boolean direction = false;
	public CollisionBox feet, head, left, right;
	double ex, ey;
	int x, y;
	int health;
	int ticks = 0;
	
	public Entity(int c1, int c2, int c3, int tilex, int tiley, Art s,int x,int y, int width, int height) {
		super(c1, c2, c3, tilex, tiley, s, 15, 15);
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.ex = x;
		this.ey = y;
		this.feet = new CollisionBox(0,0,0,0);
		this.head = new CollisionBox(0,0,0,0);
		this.left = new CollisionBox(0,0,0,0);
		this.right = new CollisionBox(0,0,0,0);
	}
	
	public Entity(Entity e)
	{
		super(e.c1, e.c2, e.c3, e.tilex, e.tiley, e.s, 15,15);
		this.setEx(e.ex);
		this.setEy(e.ey);
		this.x = e.x;
		this.y = e.y;
		this.fall = e.fall;
		this.acceleration = e.acceleration;
		this.falling = e.falling;
		this.feet = new CollisionBox(e.feet.x,e.feet.y,e.feet.width,e.feet.height);
		this.left = new CollisionBox(e.left.x,e.left.y,e.left.width,e.left.height);
		this.right = new CollisionBox(e.right.x,e.right.y,e.right.width,e.right.height);
		this.head = new CollisionBox(e.head.x,e.head.y,e.head.width,e.head.height);
		this.moving_left = e.moving_left;
		this.moving_right = e.moving_right;
		this.xVel = e.xVel;
		this.width = e.width;
		this.height = e.height;
		
	}
	
	public void tick()
	{
		x = (int)Math.round(ex);
		y = (int)Math.round(ey);
		
		/*this.head.setBounds(this.x, this.y, this.width, (int)(this.height*0.2));
		this.left.setBounds(this.x, this.y, (int)(this.width*0.2), this.height);
		this.right.setBounds((int) (this.x + this.width*0.8), this.y, (int)(this.width*0.2), this.height);
		this.feet.setBounds(this.x, (int)(this.y+this.height*0.8), this.width, (int)(this.height*0.3));*/
		
		this.head.setBounds(this.x, this.y, this.width, (int)(this.height*0.2));
		this.left.setBounds(this.x, this.y + this.height*0.15, this.width*0.2, this.height*0.7);
		this.right.setBounds(this.x + this.width*0.8, this.y + this.height*0.15, this.width*0.2, this.height*0.7);
		this.feet.setBounds(this.x, this.y+this.height*0.8, this.width, this.height*0.3);
		
		if(xVel != 0)
			xVel *= 0.9;
		
	}
	
	public void move()
	{
		if ( falling )
		{
			ey += fall;
			fall += acceleration;
		}
		else
		{
			fall = 0.2f;
		}
		
		if(moving_left)
		{
			if(xVel > 0.1)
				xVel *= 0.98;
			else
			{
				xVel = -this.speed;
				this.direction = false;
			}
		}

		if(moving_right)
		{
			if(xVel < -0.1)
				xVel *= 0.98;
			else
			{
				xVel = this.speed;
				this.direction = true;
			}
		}
		
		this.ex += xVel;
		
	}

	
	public int getX() { return x;}
	public int getY() { return y;}
	
	public double getEx() { return ex;}
	public double getEy() { return ey;}
	 
	public int getWidth() { return this.width; }
	public int getHeight() { return this.height; }
	
	public void setEy(double Ey) { this.ey = Ey; }
	public void setEx(double Ex) { this.ex = Ex; }
	
	
	public void render(Screen s, int offsetX, int offsetY)
	{
		s.drawEntity(this, x-offsetX, y-offsetY, direction );
	}

}
