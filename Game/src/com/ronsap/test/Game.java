package com.ronsap.test;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import com.ronsap.test.level.Level;
import com.ronsap.test.units.map.Map;

public class Game extends Canvas implements Runnable
{

	private static final long serialVersionUID = 1L;
	
	public static final int WIDTH  = 200;
	public static final int HEIGHT = 200;
	public static final int SCALE = 5;
	
	public boolean isrunning = false;
	public Screen mainScreen;
	public Level level;
	public KeyManager keym;
	
	int x = 10, y = 50, xs = 1, ys = 1, t;
	
	public Game()
	{
		keym = new KeyManager(this);
		mainScreen = new Screen(WIDTH, HEIGHT);
		level = new Level(mainScreen, new Map(50,50));
		this.addKeyListener(keym);
		this.setFocusable(true);
	}
	
	public synchronized void start()
	{
		if(isrunning) {return;}
		isrunning = true;
		new Thread(this).start();
	}
	
	public static void main(String[] args)
	{
		System.out.println(String.format("0x%08X", 0x00ff00));
		System.out.println(String.format("0x%08X", 0xff00ffff >>> 24));
		Game game = new Game();
		
		JFrame frame = new JFrame();
		frame.add(game);
		frame.setSize(WIDTH*SCALE, HEIGHT*SCALE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		game.start();
	}
	
	public void tick()
	{
		level.tick();
	}
	
	public void render()
	{
		BufferStrategy bs = this.getBufferStrategy();
		
		if(bs == null)
		{
			this.createBufferStrategy(2);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
		mainScreen.clear(Color.black.getRGB());
		level.render();
		g.drawImage(mainScreen.getScreenFrame(), 0,0,WIDTH*SCALE,HEIGHT*SCALE,null);
		
		g.dispose();
		bs.show();
		
	}

	@Override
	public void run() {
		
		while(isrunning)
		{
			tick();
			render();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	

}
