package com.ronsap.test;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import com.ronsap.test.gfx.Sprite;
import com.ronsap.test.units.entities.Entity;

public class Bitmap 
{
	private int width,height;
	private int[] pixels;
	
	public Bitmap(int w, int h)
	{
		pixels = new int[w*h];
		this.width = w;
		this.height = h;
	}
	
	public Bitmap(int w, int h, int[] pixels)
	{
		this.width = w;
		this.height = h;
		this.pixels = pixels;
	}
	
	public Bitmap(BufferedImage b)
	{
		this.width = b.getWidth();
		this.height = b.getHeight();
		
		this.pixels = (((DataBufferInt)(b.getRaster().getDataBuffer())).getData());
		
	}
	
	public void fillRect(Rectangle r, int color)
	{
		int x = r.x;
		int y = r.y;
		int width = r.width;
		int height = r.height;
		
		for(int xx = 0; xx < width; xx++)
		{
			for(int yy = 0; yy < height; yy++)
			{
				int nx = (x+xx);
				int ny = (y+yy);
				if((nx >= 0 && nx < this.getWidth()) && (ny >= 0 && ny < this.getHeight()))
				{
					pixels[(ny*this.getWidth())+nx] = color;
				}
			}
		}
	}
	

	public void fillRect(Rectangle r, double xScroll, double yScroll, int color) {
		int x = (int)(r.x - xScroll);
		int y = (int)(r.y - yScroll);
		int width = r.width;
		int height = r.height;
		
		for(int xx = 0; xx < width; xx++)
		{
			for(int yy = 0; yy < height; yy++)
			{
				int nx = (x+xx);
				int ny = (y+yy);
				if((nx >= 0 && nx < this.getWidth()) && (ny >= 0 && ny < this.getHeight()))
				{
					pixels[(ny*this.getWidth())+nx] = color;
				}
			}
		}
	}
	
	public void fillRect(int x, int y, int width, int height, int color)
	{
		
		for(int xx = 0; xx < width; xx++)
		{
			for(int yy = 0; yy < height; yy++)
			{
				int nx = (x+xx);
				int ny = (y+yy);
				if((nx >= 0 && nx < this.getWidth()) && (ny >= 0 && ny < this.getHeight()))
				{
					pixels[(ny*this.getWidth())+nx] = color;
				}
			}
		}
	}
	
	public void drawImage(Bitmap b, int x, int y)
	{
		for(int nx = x; nx < x + b.getWidth(); nx++)
		{
			for(int ny = y; ny < y+b.getHeight(); ny++)
			{
				if((nx >= 0 && nx < this.getWidth()) && (ny >= 0 && ny < this.getHeight()))
				{
					pixels[(ny*this.getWidth())+nx] = b.getPixel(nx-x, ny-y);
				}
			}
		}
	}
	
	public void drawSprite(Sprite s, int x, int y, boolean flipx)
	{
		Bitmap b = s.s.subframes[s.tilex+s.frame][s.tiley];
		if(flipx)
			b = b.flipX();
		
		for(int nx = x; nx < x + b.getWidth(); nx++)
		{
			for(int ny = y; ny < y+b.getHeight(); ny++)
			{
				if((nx >= 0 && nx < this.getWidth()) && (ny >= 0 && ny < this.getHeight()))
				{
					int col = b.getPixel(nx-x, ny-y);
					int ncl = 0x00000000;
					if(col == Sprite.C1)
						ncl = s.c1;
					if(col == Sprite.C2)
						ncl = s.c2;
					if(col == Sprite.C3)
						ncl = s.c3;

					double alpha = 255.0/((ncl >>> 24));
					if(alpha != 1.0)
						alpha = 0.0;
					int curr_color = pixels[(ny*this.getWidth())+nx];
					int nclor = (int)((curr_color * (1-alpha)) + (ncl * (alpha)));
					pixels[(ny*this.getWidth())+nx] = nclor;
				}
			}
		}
		
	}
	
	public void drawEntity(Entity e, int x, int y, boolean flipx)
	{
		Bitmap b = e.s.subframes[e.tilex+e.frame][e.tiley];
		if(flipx)
			b = b.flipX();
		
		BufferedImage b2 = new BufferedImage(e.swidth, e.sheight, BufferedImage.TYPE_INT_ARGB);
		b2.setRGB(0, 0, e.swidth, e.sheight, b.getPixels(), 0, e.swidth);

		BufferedImage b3 = new BufferedImage(e.width, e.height, BufferedImage.TYPE_INT_ARGB);
		b3.getGraphics().drawImage(b2, 0, 0, e.width, e.height, null);
		
		b = new Bitmap(b3);
		
		for(int nx = x; nx < x + b.getWidth(); nx++)
		{
			for(int ny = y; ny < y+b.getHeight(); ny++)
			{
				if((nx >= 0 && nx < this.getWidth()) && (ny >= 0 && ny < this.getHeight()))
				{
					int col = b.getPixel(nx-x, ny-y);
					int ncl = 0x00000000;
					if(col == Sprite.C1)
						ncl = e.c1;
					if(col == Sprite.C2)
						ncl = e.c2;
					if(col == Sprite.C3)
						ncl = e.c3;

					double alpha = 255.0/((ncl >>> 24));
					if(alpha != 1.0)
						alpha = 0.0;
					int curr_color = pixels[(ny*this.getWidth())+nx];
					int nclor = (int)((curr_color * (1-alpha)) + (ncl * (alpha)));
					pixels[(ny*this.getWidth())+nx] = nclor;
				}
			}
		}
		
	}
	
	public Bitmap flipX()
	{
		Bitmap b = new Bitmap(this.width, this.height);
		
		for(int i = 0; i < this.width; i++)
		{
			for(int j = 0; j < this.height; j++)
			{
				b.setPixel(this.width-i-1, j, this.getPixel(i, j));
			}
		}
		
		return b;
	}
	
	public void clear(int color)
	{for (int i = 0; i < pixels.length; i++) { pixels[i] = color;}}
	
	public int getWidth()
	{ return width; }
	
	public int getHeight()
	{ return height; }
	
	public int[] getPixels()
	{ return pixels; }
	
	public void setPixels(int[] pixels)
	{ this.pixels = pixels; }
	
	public int getPixel(int x, int y)
	{ return pixels[(y*this.getWidth())+x]; }
	
	public void setPixel(int x, int y, int color)
	{ pixels[(y*this.getWidth())+x] = color; }
}
