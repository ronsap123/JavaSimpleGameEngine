package com.ronsap.test.units.entities;

import java.util.Random;

import com.ronsap.test.Art;

public class Player extends Entity
{
	
	public Player(int x,int y, int width, int height) {
		super(0xff000000 + new Random().nextInt(0x00ffffff),
				0xff000000 + new Random().nextInt(0x00ffffff),
				0xff000000 + new Random().nextInt(0x00ffffff),0,0,Art.entitySheet,x,y,width,height);
		this.health = 250;
		this.speed = 1.5f;
	}
	
	public Player(Player p) {
		super(p);
	}

	public void tick()
	{
		super.move();
		super.tick();
		
		if(this.moving_left || this.moving_right)
		{
			ticks += 1;
			if(ticks == 120)
			{
				ticks = 0;
			}
			this.frame = (int)(1 + (ticks/10)%3);
		}
		else
		{
			this.frame = 0;
		}
		

		if(moving_left)
		{
			if(xVel > 0.1)
				this.frame = 6;
		}

		if(moving_right)
		{
			if(xVel < -0.1)
				this.frame = 6;
			
		}
		
		if(this.falling)
		{
			this.frame = 4;
		}
		
		
	}
	
	
	
	/*public Player clone()
	{
		Player p = new Player(this.x,this.y,width,height);
		p.setEx(this.ex);
		p.setEy(this.ey);
		p.fall = this.fall;
		p.acceleration = this.acceleration;
		p.falling = this.falling;
		p.feet = new CollisionBox(this.feet.x,this.feet.y,this.feet.width,this.feet.height);
		p.left = new CollisionBox(this.left.x,this.left.y,this.left.width,this.left.height);
		p.right = new CollisionBox(this.right.x,this.right.y,this.right.width,this.right.height);
		p.head = new CollisionBox(this.head.x,this.head.y,this.head.width,this.head.height);
		p.moving_left = this.moving_left;
		p.moving_right = this.moving_right;
		return p;
	}*/
	
	public void loadState(Player p)
	{
		this.ex = p.getEx();
		this.ey = p.getEy();
		this.x = p.getX();
		this.y = p.getY();
		
		this.fall = p.fall;
		this.acceleration = p.acceleration;
		this.falling = p.falling;
	}
	
	
	
	public void setFalling(boolean f) {this.falling = f; System.out.println("test");}
	
}
