package com.ronsap.test.units.map;

import java.util.Random;

import com.ronsap.test.Properties;
import com.ronsap.test.units.entities.Entity;
import com.ronsap.test.units.map.blocks.Air;
import com.ronsap.test.units.map.blocks.Brick;
import com.ronsap.test.units.map.blocks.Dirt;
import com.ronsap.test.units.map.blocks.Stone;


public class Map 
{
	
	int size_w, size_h;
	Block[][] block_matrix;
	
	public Map(int size_w, int size_h)
	{
		this.size_w = size_w;
		this.size_h = size_h;
		
		block_matrix = new Block[size_w][size_h];
		for(int i = 0; i < size_w; i++)
		{
			for(int j = 0; j < size_h; j++)
			{
				int type = new Random().nextInt(5);
				if(type == 0)
					block_matrix[i][j] = new Dirt(i,j);

				if(type == 1)
					block_matrix[i][j] = new Stone(i,j);

				if(type == 2)
					block_matrix[i][j] = new Brick(i,j);
				
				if(type == 3 || type == 4)
					block_matrix[i][j] = new Air(i,j);
			}
		}
	}
	
	public void handleEntityMovement(Entity e)
	{

		Entity e1 = new Entity(e);
		//e1.speed *= 0.3;
		e1.tick();
		
		
		int height_blocks = (int) (e.right.height/15.0);
		
		for(int ay = 0; ay < height_blocks+2; ay++)
		{
			Block left = this.blockAt((int)Math.round(e1.getEx()/(float)Properties.BLOCK_SIZE) -1, (int)Math.round(e1.getEy()/(float)Properties.BLOCK_SIZE) + ay-1);
			if( !(left instanceof Air || left == null) && e1.left.isColliding( left ))
			{
				if(e.xVel < 0)
				{
					e.moving_left = false;
					e.setEx((left.getX()+1)*left.getWidth() - ( e.getWidth() - left.getHeight() ));
				}
			}
		}
		
		
		for(int ay = 0; ay < height_blocks+2; ay++)
		{
			Block right = this.blockAt((int)Math.round(e1.getEx()/(float)Properties.BLOCK_SIZE) + 1, (int)Math.round(e1.getEy()/(float)Properties.BLOCK_SIZE)+ay-1);
			if( !(right instanceof Air || right == null) && e1.right.isColliding( right ))
			{
				if(e.xVel > 0)
				{
					e.moving_right = false;
					e.setEx((right.getX()-1)*right.getWidth() - ( e.getWidth() - right.getHeight() ));
				}
			}
		}
		
		Block beneath = this.blockAt( (int)Math.round(e.feet.getX()/(float)Properties.BLOCK_SIZE) , (int)Math.round(e.feet.getY()/(float)Properties.BLOCK_SIZE) );

		
		if( !(beneath instanceof Air || beneath == null) && e1.feet.isColliding( beneath ))
		{
			if(e.falling && e.fall > 0)
			{
				e.falling = false;
				if(this.blockAt(beneath.getX(), beneath.getY()-1) instanceof Air || this.blockAt(beneath.getX(), beneath.getY()-1) == null )
					e.setEy((beneath.getY()-1)*beneath.getHeight() - ( e.getHeight() - beneath.getHeight() ));
			}
		}
		
		if(beneath instanceof Air || beneath == null)
		{
			e.falling = true;
		}
		
		
		Block above = this.blockAt( (int)Math.round(e1.getEx()/(float)Properties.BLOCK_SIZE) , (int)Math.round(e1.getEy()/(float)Properties.BLOCK_SIZE)-1 );

		
		if( !(above instanceof Air || above == null) && e1.head.isColliding( above ))
		{
			if(e.falling && e.fall < 0)
			{
				e.fall = 0;
				if(this.blockAt(above.getX(), above.getY()-1) instanceof Air || this.blockAt(above.getX(), above.getY()-1) == null )
					e.setEy((above.getY()+1)*above.getHeight() - ( e.getHeight() - above.getHeight() ));
			}
		}
		
		
	}
	
	public int getWidth()
	{
		return size_w;
	}
	
	public int getHeight()
	{
		return size_h;
	}
	
	public Block blockAt(int x, int y)
	{
		if(x < 0 || x >= size_w || y < 0 || y >= size_h)
		{
			return null;
		}
		return block_matrix[x][y];
	}
	
}
