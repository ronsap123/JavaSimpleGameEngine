package com.ronsap.test.units.map.blocks;

import com.ronsap.test.units.map.Block;

public class Stone extends Block
{
	public Stone(int x, int y)
	{
		super(2,0,15,15,x,y,0.4f,0xffbab4c4,0xff7a7682,0xff615e68);
	}
}
