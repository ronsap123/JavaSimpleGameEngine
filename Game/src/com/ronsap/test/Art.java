package com.ronsap.test;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Art {
	
	public static Art entitySheet = new Art("res/player_sheet.png", 15,15);
	public static Art blockSheet = new Art("res/block_sheet.png",15,15);
	public Bitmap[][] subframes;
	
	public Art(String filepath, int width, int height)
	{
		BufferedImage newImage = null;
		try {
			BufferedImage in = ImageIO.read(new File(filepath));

			newImage = new BufferedImage(
			    in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);

			Graphics2D g = newImage.createGraphics();
			g.drawImage(in, 0, 0, null);
			g.dispose();
		} catch (IOException e) {
			System.out.println("Unable to open source file");
		}
		if(newImage != null)
		{
			int cols = newImage.getWidth()/width;
			int rows = newImage.getHeight()/height;
			
			subframes = new Bitmap[cols][rows];
			
			for(int i = 0; i < rows; i++)
			{
				for(int j = 0; j < cols; j++)
				{
					int[] result = new int[height*width];
					newImage.getRGB(i*width, j*height, width, height, result, 0, width);
					subframes[i][j] = new Bitmap(width,height,result);
				}
			}
			
		}
	}
	
}
