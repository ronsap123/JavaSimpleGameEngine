package com.ronsap.test;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class KeyManager implements KeyListener{
	
	Game parent;
	
	public KeyManager(Game g)
	{
		this.parent = g;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_A)
		{
			parent.level.p.moving_left = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_D)
		{
			parent.level.p.moving_right= true;
		}
		if(e.getKeyCode() == KeyEvent.VK_W)
		{
			if(parent.level.p.fall > 0 && parent.level.p.falling == false)
			{
				parent.level.p.fall = -3f;
				parent.level.p.falling = true;
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_R)
		{
			parent.level.p.c1 = 0xff000000 + new Random().nextInt(0x00ffffff);
			parent.level.p.c2 = 0xff000000 + new Random().nextInt(0x00ffffff);
			parent.level.p.c3 = 0xff000000 + new Random().nextInt(0x00ffffff);

		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_A)
		{
			parent.level.p.moving_left = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_D)
		{
			parent.level.p.moving_right= false;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
