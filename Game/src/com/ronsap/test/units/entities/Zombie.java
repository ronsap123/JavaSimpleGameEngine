package com.ronsap.test.units.entities;

import java.util.Random;

import com.ronsap.test.Art;
import com.ronsap.test.gfx.ColorHelper;
import com.ronsap.test.units.CollisionBox;

public class Zombie extends Entity{

	public Zombie(int x, int y, int width, int height) {
		super(ColorHelper.getColor(20, 230, 50, 255), ColorHelper.getColor(100, 255, 50, 255), ColorHelper.getColor(200, 20, 200, 255), 0, 1, Art.entitySheet, x, y, width, height);
	}
	
	@Override
	public void tick()
	{
		super.move();
		super.tick();
		
		if(new Random().nextInt(150) == 0)
		{
			moving_left = true;
			moving_right = false;
		}
		if(new Random().nextInt(150) == 0)
		{
			moving_left = false;
			moving_right = true;
		}
		if(this.falling == false && new Random().nextInt(50) == 0)
		{
			this.fall = -2.0f;
			this.falling = true;
		}
		
		if(this.moving_left || this.moving_right)
		{
			ticks += 1;
			if(ticks == 120)
			{
				ticks = 0;
			}
			this.frame = (int)(0 + (ticks/10)%2);
		}
		else
		{
			this.frame = 0;
		}
		
		if(this.falling)
		{
			this.frame = 0;
		}
		
	}

	public Zombie clone()
	{
		Zombie p = new Zombie(this.x,this.y,width,height);
		p.setEx(this.ex);
		p.setEy(this.ey);
		p.fall = this.fall;
		p.acceleration = this.acceleration;
		p.falling = this.falling;
		p.feet = new CollisionBox(this.feet.x,this.feet.y,this.feet.width,this.feet.height);
		p.left = new CollisionBox(this.left.x,this.left.y,this.left.width,this.left.height);
		p.right = new CollisionBox(this.right.x,this.right.y,this.right.width,this.right.height);
		p.head = new CollisionBox(this.head.x,this.head.y,this.head.width,this.head.height);
		return p;
	}

}
