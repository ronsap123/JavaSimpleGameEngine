package com.ronsap.test.gfx;

public class ColorHelper {
		
	public static int getColor(int r, int g, int b, int a)
	{
		int color = a;
		color = color << 8;
		color += r;
		color = color << 8;
		color += g;
		color = color << 8;
		color += b;
		return color;
	}
	
}
