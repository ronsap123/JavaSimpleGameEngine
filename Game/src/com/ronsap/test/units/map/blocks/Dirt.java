package com.ronsap.test.units.map.blocks;

import com.ronsap.test.units.map.Block;

public class Dirt extends Block{
	
	public Dirt(int x, int y)
	{
		super(1,0,15,15,x,y,0.2f,0xffbccea5,0xff756559,0xff543d2c);
	}

}
