package com.ronsap.test.units.map;
import com.ronsap.test.Art;
import com.ronsap.test.gfx.Sprite;

public class Block extends Sprite
{
	int x, y, width,height;
	double health;
	double stiffness;
	public Block(int tilex, int tiley,int width, int height, int x, int y, float stiffness, int c1, int c2, int c3) {
		super(c1,c2,c3,tilex,tiley, Art.blockSheet, 15, 15);
		this.width = width;
		this.height = height;
		this.health = 100;
		this.x = x;
		this.y = y;
		this.stiffness = stiffness;
	}
	
	public void tick()
	{
	}
	
	public int getX() { return this.x;}
	public int getY() { return this.y;}
	public int getWidth() { return this.width;}
	public int getHeight() { return this.height;}

}
