package com.ronsap.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;


public class Screen extends Bitmap
{
	
	BufferedImage buffer;
	
	public Screen(int w, int h) 
	{
		super(w, h);
		buffer = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
		super.setPixels(((DataBufferInt)(buffer.getRaster().getDataBuffer())).getData());
	}
	
	public BufferedImage getScreenFrame()
	{
		return buffer;
	}

	

}
