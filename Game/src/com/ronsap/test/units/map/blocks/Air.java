package com.ronsap.test.units.map.blocks;

import com.ronsap.test.units.map.Block;

public class Air extends Block{
	
	public Air(int x, int y)
	{
		super(0,0,15,15,x,y,0.8f,0,0,0);
	}

}
